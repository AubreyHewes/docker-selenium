# Selenium

Selenium standalone/hub/node within a docker container.

Currently only phantomjs on linux. 

TODO is to separate to multiple container definitions. I.e. windows with internet explorer/chrome/etc.

 * container: selenium-hub
 * container: selenium-node-lux (phantomjs, firefox, chrome) - requires desktop
 * container: selenium-node-win (phantomjs, firefox, chrome)
 * container: selenium-node-osx (a long shot)

## Standalone

Run a standalone Selenium server

	docker run aubreyhewes/selenium
	
### Configuration

Use host http://gateway-ip:4444 as hub/node


## Selenium Grid

**NOTE: CURRENTLY NOT WORKING**: Due to wiring (ports/hosts/etc.. possible solution via linked containers)

Setup a [Grid](http://www.seleniumhq.org/docs/07_selenium_grid.jsp)

### Start a Hub

	docker run -p 4444:4444 aubreyhewes/selenium -role hub

### Start a Node

	docker run -p 44441:44441 aubreyhewes/selenium -role node -port 44441 -hub http://gateway-ip:4444/grid/register

### Start another Node

	docker run -p 44442:44442 aubreyhewes/selenium -role node -port 44442 -hub http://gateway-ip:4444/grid/register

## Components

### OS

OS is Debian due to minimalistic requirements

### Selenium

Selenium is downloaded from the source [http://www.seleniumhq.org/](http://www.seleniumhq.org/)

### Java

Java is Oracle Java 8 via http://www.webupd8.org/ installed via `apt`

A _proper_ JVM is used due to it having a higher performance and lower memory footprint.

### PhantomJS

A binary PhantomJS is installed due to otherwise having to build an own binary.

This binary is downloaded from https://bitbucket.org/ariya/phantomjs/downloads/ (copied from the npm package)...
This is currently x86_64 (container arch is not checked)

#### TODO
Find binary via container arch




