# Dockerfile for Selenium
#
# NOTE: Due to otherwise needing to build an own phantomjs we use a pre-compiled binary (based on npm installation)
#
# TODO: network sharing/linking for grid
#

# Use minimal os
FROM debian:jessie

# Make it so
RUN \
  # Fix feedback \
  export DEBIAN_FRONTEND=noninteractive && \
  \
  # Setup apt repo for oracle java \
  echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu utopic main" | tee /etc/apt/sources.list.d/webupd8team-java.list && \
  echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886 && \
  \
  # Update sources.list \
  apt-get update && \
  \
  # Upgrade current packages \
  apt-get upgrade -y --no-install-recommends && \
  \
  # Install required apt packages \
  #   ca-certificates and bzip2 are required for upcoming phantomjs installation \
  #   libfontconfig1 is required for phantomjs runtime \
  # \
  apt-get install -y --no-install-recommends \
    curl \
    nano \
    dnsutils \
    oracle-java8-installer \
    oracle-java8-set-default \
    ca-certificates \
    bzip2 \
    libfontconfig1 && \
  \
  # Install phantomjs \
  curl -Lso phantomjs.tar.bz2 https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-1.9.8-linux-x86_64.tar.bz2 && \
  tar -xjf phantomjs.tar.bz2 && \
  ln -fs /phantomjs-1.9.8-linux-x86_64/bin/phantomjs /usr/bin/phantomjs && \
  \
  # Install selenium \
  mkdir /app && \
  curl -Lso /app/selenium.jar http://selenium-release.storage.googleapis.com/2.45/selenium-server-standalone-2.45.0.jar

ENTRYPOINT ["/usr/bin/java", "-jar", "/app/selenium.jar"]